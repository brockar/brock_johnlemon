﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightBonk : MonoBehaviour
{
    /*
     * TWO big variables now: toBonk and toGrab. toBonk
     * is basically what was discussed in class. toGrab 
     * is for when there's a collectible you can pick up.
     */

    public GameObject toBonk;
    public GameObject toGrab;
    public GameObject player;
    int charges;
    Light lightSource;
    bool canBonk = false;
    public float flashTime = 3f;
    public float maxIntensity = 500;

    /*
     * NOTES: Maybe add controls display before playing?
     */

    private void Start()
    {
        charges = 3;
        lightSource = GetComponent<Light>();
        lightSource.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            FlashLight();
        }
        if (Input.GetKeyDown(KeyCode.Space) && toGrab != null)
        {
            //when you get treasure, you get another charge
            charges += 1;
            player.GetComponent<PlayerMovement>().GetTreasure();
            Destroy(toGrab.gameObject);
        }
    }

    /*
     * Fills variables for bonking/grabbing.
     * Basically checking if they're in range.
     */
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ghost"))
        {
            toBonk = other.gameObject;
        }
        if (other.CompareTag("Treasure"))
        {
            toGrab = other.gameObject;
        }

        if (canBonk && toBonk != null)
        {
            toBonk.GetComponent<WaypointPatrol>().Stun();
        }
    }

    /*
     * Opposite of OnTriggerEnter. When the bonkee
     * or grabee leaves, they are taken out of the variable.
     */
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Ghost"))
        {
            if (toBonk == other.gameObject)
            {
                toBonk = null;
            }
        }
        if (other.CompareTag("Treasure"))
        {
            if (toGrab == other.gameObject)
            {
                toGrab = null;
            }
        }
    }

    /*
     * Enables flashing eyes and activates bonk area.
     */
    private void FlashLight()
    {
        if (!lightSource.enabled && (charges > 0))
        {
            charges -= 1;
            lightSource.enabled = true;
            canBonk = true;
        }
        Invoke("LightsOut", 2f);
    }

    /*
     * Disables flashing eyes and deactivates bonk area.
     */
    private void LightsOut()
    {
        lightSource.enabled = false;
        canBonk = false;
    }

    /*
     * Reports current # of charges. Used for UI.
     */
    public int UpdateCharges()
    {
        return charges;
    }
}
