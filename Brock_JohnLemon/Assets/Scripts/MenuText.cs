﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuText : MonoBehaviour
{
    public string displayText;

    public GameObject dialPanel; //location of panel

    bool dialOpened;
    bool canClose;

    /*
     * Largely recycled from TombText, but using Escape
     * to feel more like a menu. Also starts the game.
     * We have this to tell the player some story n stuff.
     */

    private void Start()
    {
        Time.timeScale = 0f; //game is paused to start
        OpenDialPanel();
    }

    // Update is called once per frame
    void Update()
    {
        //escape needs to be tapped twice to exit or open
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!dialOpened)
            {
                if (canClose)
                {
                    canClose = false;
                }
                else
                {
                    OpenDialPanel();
                }
            }
            else if (dialOpened)
            {
                if (!canClose)
                {
                    canClose = true;
                }
                else
                {
                    CloseDialPanel();
                }
            }
        }
    }

    /*
     * Opens menu and freezes time.
     */
    void OpenDialPanel()
    {
        dialOpened = true;
        Time.timeScale = 0f; //freezes time

        dialPanel.SetActive(true);

        canClose = false;
    }

    /*
     * Closes menu and restarts time.
     */
    void CloseDialPanel()
    {
        dialOpened = false;
        Time.timeScale = 1f;

        dialPanel.SetActive(false);
        canClose = true;
    }
}
