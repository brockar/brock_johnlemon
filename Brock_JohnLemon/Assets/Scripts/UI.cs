﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI : MonoBehaviour
{
    public GameObject player;
    public GameObject lightScore;
    public GameObject scoreText;
    public GameObject HPText;
    public GameObject chargeText;
    public GameObject doneText;
    private int treasure;
    private int hp;
    private int charges;
    private int goal;

    // Start is called before the first frame update
    void Start()
    {
        treasure = player.GetComponent<PlayerMovement>().UpdateTreasure();
        hp = player.GetComponent<PlayerMovement>().UpdateHealth();
        charges = lightScore.GetComponent<LightBonk>().UpdateCharges();
        goal = player.GetComponent<PlayerMovement>().UpdateGoal();
    }

    // Update is called once per frame
    /*
     * Updates each value from player so UI is always correct.
     */
    void Update()
    {
        treasure = player.GetComponent<PlayerMovement>().UpdateTreasure();
        if (treasure == goal)
        {
            //If the player has the right amount of treasure, the game tells them they can go.
            doneText.GetComponent<UnityEngine.UI.Text>().text = "You have all the treasure! Go back to the start to leave!";
        }
        hp = player.GetComponent<PlayerMovement>().UpdateHealth();
        charges = lightScore.GetComponent<LightBonk>().UpdateCharges();
        scoreText.GetComponent<UnityEngine.UI.Text>().text = "Treasure: " + treasure;
        HPText.GetComponent<UnityEngine.UI.Text>().text = "HP: " + hp;
        chargeText.GetComponent<UnityEngine.UI.Text>().text = "Charges: " + charges;
    }
}
