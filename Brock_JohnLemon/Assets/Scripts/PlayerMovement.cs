﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class PlayerMovement : MonoBehaviour
{
    float turnSpeed = 20f;
    public int healthPoints;
    public int treasure; //treasures currently acquired
    public int goal; //treasures needed to win game
    public GameEnding ending; //for triggering ending upon player death

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement; //new naming convention! "m_" denotes a non-public member
    Quaternion m_Rotation = Quaternion.identity;

    public float moveSpeed;
    /*
     * ^^ a way of storing rotations; they get around some of the problems that
     * come up when you store them as a 3D vector
     */

    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>(); //snags animator from john
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
    }

    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize(); //keeps the vector's direction the same, changes magnitude to 1
        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        //Approximately() takes two floats and returns a bool; true if they're approx. equal
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);
        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);

        //these two were added because john lemon has whacky movement, actually
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * moveSpeed * Time.deltaTime);
        transform.rotation = m_Rotation;
    }

    /*
     * Called from Observer, takes HP away from player.
     * If out of HP, the game ends.
     */
    public void TakeDamage()
    {
        healthPoints -= 1;
        if (healthPoints <= 0)
        {
            ending.CaughtPlayer();
        }
    }

    /*
     * Increments treasure variable (# of collectibles) and 
     * reports how many more player needs to collect. When all
     * are collected, reports to player to get to exit.
     */
    public void GetTreasure()
    {
        treasure += 1;
        if (treasure == goal)
        {
            ending.GoalAchieved();
        }
    }

    /*
     * Reports current treasure count. Helper function
     * for UI.
     */
    public int UpdateTreasure()
    {
        return treasure;
    }

    /*
     * Reports current HP count. Helper function for
     * UI.
     */
    public int UpdateHealth()
    {
        return healthPoints;
    }

    public int UpdateGoal()
    {
        return goal;
    }
}
