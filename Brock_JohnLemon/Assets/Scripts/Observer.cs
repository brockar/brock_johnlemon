﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Observer : MonoBehaviour
{
    public Transform player;
    public GameObject playerObject;
    bool m_IsPlayerInRange;

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform == player)
        {
            playerObject = other.gameObject;
            m_IsPlayerInRange = true;
            HurtPlayer();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = false;
        }
    }

    /*
     * Recycled from tutorial's 'Update()' function.
     * Checks if Player is in range of POV, then hurts
     * Player by accessing the TakeDamage() function of
     * Player's main script.
     * 
     * NOTE: Moved from Update() because otherwise the 
     * player kept getting hurt for every frame they were 
     * in the POV, which... was not good.
     */
    private void HurtPlayer()
    {
        if (m_IsPlayerInRange)
        {
            playerObject.GetComponent<PlayerMovement>().TakeDamage();
        }
    }
}
