﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public GameObject gate;
    public GameObject player;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public AudioSource exitAudio;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource caughtAudio;
    bool m_IsPlayerAtExit;
    bool m_IsPlayerCaught;
    float m_Timer;
    bool m_HasAudioPlayed;

    bool m_CanPlayerWin; //player needs set amount of treasure before they can get through

    /*
     * So we know when the player is actually there.
     */
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            m_IsPlayerAtExit = true;
        }
    }

    /*
     * Opposite of OnTriggerEnter. Prevents tomfoolery.
     */
    private void OnTriggerExit(Collider other)
    {
        m_IsPlayerAtExit = false;
    }

    /*
     * Called from PlayerMovement. If player's caught by ghosts,
     * we need to give them that game over.
     */
    public void CaughtPlayer()
    {
        m_IsPlayerCaught = true;
    }

    /*
     * Called from PlayerMovement. If player has all the treasure,
     * the gate closing them in leaves and they can walk away.
     */
    public void GoalAchieved()
    {
        m_CanPlayerWin = true;
        Destroy(gate);
    }

    private void Update()
    {
        if (m_IsPlayerAtExit)
        {
            /*
             * Player needs to collect all collectibles before they
             * can properly leave now. This is where that's checked.
             */
            if (m_CanPlayerWin)
            {
                EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);
            }
            else //need to display text saying you need more pumpkins... dialogue?
            {
                print("You need more pumpkins!");
            }
        }
        else if (m_IsPlayerCaught)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }
    }

    /*
     * Ends the level with appropriate music and not so appropriate artwork.
     */
    private void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        if (!m_HasAudioPlayed)
        {
            audioSource.Play();
            m_HasAudioPlayed = true;
        }
        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;
        if (m_Timer > (fadeDuration + displayImageDuration))
        {
            if (doRestart)
            {
                SceneManager.LoadScene(0); //0 is the index of mainscene
            }
            else
            {
                Application.Quit(); //won't work until we build the game
            }
        } 
    }
}
