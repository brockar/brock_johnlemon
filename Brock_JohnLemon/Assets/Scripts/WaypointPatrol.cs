﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WaypointPatrol : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;
    public Transform[] waypoints;
    int m_CurrentWaypointIndex;

    bool isStunned = false;
    public GameObject povObject;

    void Start()
    {
        navMeshAgent.SetDestination(waypoints[0].position);
    }

    /*
     * Stuns the ghost and makes them incapacitated for 3 seconds.
     * Public so we can reference it from LightBonk.
     */
    public void Stun() //public so we can reference it from LightBonk
    {
        if (!isStunned)
        {
            isStunned = true;
            navMeshAgent.isStopped = true; //stops moving
            povObject.SetActive(false); //can't see
            Invoke("ResumePatrol", 3f); //starts again in 3s
        }
    }

    /*
     * Resumes patrol of ghost.
     */
    public void ResumePatrol()
    {
        isStunned = false;
        navMeshAgent.isStopped = false;
        povObject.SetActive(true);
    }

    /*
     * Keeps the boy moving.
     */
    void Update()
    {
        if (navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance)
        {
            m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length; //basically a manual loop here
            navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
        }
    }
}
