﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TombText : MonoBehaviour
{
    public string displayText;

    public GameObject dialPanel; //location of panel
    public Text dialText; //location of text

    bool dialOpened;
    bool canClose = true;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (dialOpened)
            {
                if (!canClose)
                {
                    canClose = true;
                } else
                {
                    CloseDialPanel();
                }
            }
        }
    }

    /*
     * On clicking object, opens its dialogue.
     */
    private void OnMouseDown()
    {
        if (!dialOpened)
        {
            OpenDialPanel();
        }
    }

    /*
     * Opens dialogue and freezes time.
     */
    void OpenDialPanel()
    {
        dialOpened = true;
        Time.timeScale = 0f; //freezes time

        dialPanel.SetActive(true);
        dialText.text = displayText;

        canClose = false;
    }

    /*
     * Closes dialogue and resumes time.
     */
    void CloseDialPanel()
    {
        dialOpened = false;
        Time.timeScale = 1f;

        dialPanel.SetActive(false);
    }
}
